package nl.yeswayit.jsonrest.data.repo;

import nl.yeswayit.jsonrest.data.tables.Webuser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WebuserRepo extends JpaRepository<Webuser, String> {
    @Query("SELECT t FROM Webuser t where voornaam like ?1 AND achternaam like ?2 AND userid <> ?3")
    Optional<List<Webuser>> searchUser(String voornaam, String achternaam, String excudeUserId);
}
