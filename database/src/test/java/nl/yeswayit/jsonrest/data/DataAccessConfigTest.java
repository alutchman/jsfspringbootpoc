package nl.yeswayit.jsonrest.data;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.jmx.export.MBeanExporter;

import javax.naming.Context;
import javax.sql.DataSource;

import static nl.yeswayit.jsonrest.data.DataAccessConfig.DATA_SOURCE_NAME;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class DataAccessConfigTest {
    private static DataSource dataSource = new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.H2).setName("AuditLog")
            .build();

    @Mock
    private MBeanExporter mBeanExporter ;


    @InjectMocks
    private DataAccessConfig dataAccessConfig = new DataAccessConfig();

    @BeforeClass
    public static void init(){
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                InitialContextFactoryForTest.class.getName());

        InitialContextFactoryForTest.bind(DATA_SOURCE_NAME, dataSource);

    }

    @Test
    public void getDataSource() {
        DataSource fecthed =  dataAccessConfig.getDataSource();
        assertNotNull(fecthed);
        assertEquals(dataSource, fecthed);
    }
}