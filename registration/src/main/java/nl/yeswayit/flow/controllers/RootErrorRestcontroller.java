package nl.yeswayit.flow.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;


@RestController
public class RootErrorRestcontroller implements ErrorController {
    private boolean debug = false;




    @Autowired
    private ErrorAttributes errorAttributes;

    @RequestMapping(value = "/error")
    public ModelAndView error(Model model,WebRequest webRequest) {
        Map<String, Object> errorInfo = errorAttributes.getErrorAttributes(webRequest, debug);
        return new ModelAndView("error",errorInfo);

    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
