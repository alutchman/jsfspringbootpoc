package nl.yeswayit.boot;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.naming.Context;

import static nl.yeswayit.jsonrest.data.DataAccessConfig.DATA_SOURCE_NAME;

@ComponentScan(
        basePackages = {"nl.yeswayit.config"}
)
@SpringBootApplication
@Slf4j
public class JsfSpringBootPoc  {
   static {
       System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                InitialContextFactoryForBoot.class.getName());

       BasicDataSource dataSource = new BasicDataSource();


       dataSource.setDriverClassName("org.h2.Driver");
       dataSource.setUrl("jdbc:h2:mem:yeswayit_prod");
       dataSource.setUsername("sa");
       dataSource.setPassword("");
       InitialContextFactoryForBoot.bind(DATA_SOURCE_NAME, dataSource);
    }


    public JsfSpringBootPoc(){

    }

    public static void main(String[] args) {



       SpringApplication.run(JsfSpringBootPoc.class, args);
    }

}
