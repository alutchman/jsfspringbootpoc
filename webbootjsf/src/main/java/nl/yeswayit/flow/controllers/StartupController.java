package nl.yeswayit.flow.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
public class StartupController {
    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping(value="/message.html", method = RequestMethod.GET)
    public ModelAndView loginGet(HttpServletRequest request, HttpServletResponse response){
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("baseUrl", applicationContext.getApplicationName());
        return new ModelAndView("demo/index",datamap);
    }

    @RequestMapping(value="/locatie.html", method = RequestMethod.GET)
    public ModelAndView findpostcode(){
        return new ModelAndView("demo/initloc");
    }
}
