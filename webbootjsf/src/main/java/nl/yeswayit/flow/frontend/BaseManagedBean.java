package nl.yeswayit.flow.frontend;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import nl.yeswayit.shared.transfer.UserLevel;
import nl.yeswayit.shared.transfer.WebUserData;
import nl.yeswayit.utils.FacesUtilsManagedBean;
import nl.yeswayit.utils.SessionWallet;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

@Slf4j
@Data
public abstract class BaseManagedBean implements Serializable {
    private WebUserData webUserData;

    private String contextPath;

    private String activeMenuDef;

    private String viewTitle;

    private boolean beheerder;

    private SessionWallet sessionWallet;

    protected abstract void handleInit();

    @PostConstruct
    protected final void initialize() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String loginURI = request.getContextPath() + "/login.html";
        String rootUri  = request.getContextPath() + "/";

        boolean loginRequest = request.getRequestURI().equals(rootUri) || request.getRequestURI().equals(loginURI) ;

        boolean loggedIn = FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get(WebUserData.class.getSimpleName()) != null;

        if (!loggedIn && !loginRequest ) {
            try {
                HttpServletResponse response = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
                response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
                response.sendRedirect(loginURI);
            } catch (Exception e) {
                log.error("Redirect mislukt...",e);
                throw new RuntimeException("U mag hier NOOOIT komen.....");
            }catch (Error e) {
                log.error("Redirect mislukt...",e);
                throw new RuntimeException("U mag hier NOOOIT komen.....");
            }
        } else {
            contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();

            webUserData = (WebUserData) FacesContext.getCurrentInstance().
                    getExternalContext().getSessionMap().get(WebUserData.class.getSimpleName());

            beheerder = webUserData.getCurrentlevel().equals(UserLevel.BEHEER);

            sessionWallet = (SessionWallet) FacesContext.getCurrentInstance().
                    getExternalContext().getSessionMap().get(SessionWallet.class.getSimpleName());
            handleInit();
        }
    }

    protected final <T> T getSpringBean(Class<T> aclass) {
        return FacesUtilsManagedBean.getSpringBean(aclass);
    }
}
