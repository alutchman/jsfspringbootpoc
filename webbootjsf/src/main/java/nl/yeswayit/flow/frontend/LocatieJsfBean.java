package nl.yeswayit.flow.frontend;

import lombok.Getter;
import lombok.Setter;
import nl.yeswayit.flow.services.LocatieService;
import nl.yeswayit.jsonrest.data.tables.Locaties;
import org.primefaces.component.datatable.DataTable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ViewScoped
@ManagedBean
public class LocatieJsfBean extends BaseManagedBean {
    private static final long serialVersionUID = -4086179544257223044L;

    private transient LocatieService locatieService;

    private String postcode;

    private DataTable payerTable;

    private List<Locaties> foundLocations= new ArrayList<>();

    @Override
    protected void handleInit() {
        locatieService = getSpringBean(LocatieService.class);
    }

    public void fetchtData(){
        foundLocations = locatieService.findEntity(postcode);
    }
}