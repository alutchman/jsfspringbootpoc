package nl.yeswayit.flow.frontend;

import lombok.Getter;
import nl.yeswayit.flow.services.MessageService;

import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

@Getter
@ViewScoped
@ManagedBean
public class TestJsfBean extends BaseManagedBean{

    private static final long serialVersionUID = -3361107559319237495L;
    private transient MessageService messageService;

    private String message;

    @Override
    protected void handleInit() {
        messageService = getSpringBean(MessageService.class);
        message = messageService.getMsg();

    }

}