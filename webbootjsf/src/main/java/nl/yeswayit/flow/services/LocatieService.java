package nl.yeswayit.flow.services;


import lombok.extern.slf4j.Slf4j;
import nl.yeswayit.jsonrest.data.repo.LocatiesRepo;
import nl.yeswayit.jsonrest.data.tables.Locaties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class LocatieService  {
    @Autowired
    private LocatiesRepo locatiesRepo;

    @Transactional
    @PostConstruct
    public void init(){
        Optional<List<Locaties>> optloc = locatiesRepo.findLocatie("2907DD");
        if (!optloc.isPresent()){
            Locaties loc1 = new Locaties();
            loc1.setPostcode("2907DD");
            loc1.setNummer(11);
            loc1.setToevoeging("");
            loc1.setStraatnaam("Grafiek");
            loc1.setWoonplaats("Capelle aan den Ijjssel");
            locatiesRepo.saveAndFlush(loc1);
        }

        Optional<List<Locaties>> optloc2 = locatiesRepo.findLocatie("2807EJ");
        if (!optloc2.isPresent()){
            Locaties loc2 = new Locaties();
            loc2.setPostcode("2807EJ");
            loc2.setNummer(54);
            loc2.setToevoeging("");
            loc2.setStraatnaam("Gortmolenerf");
            loc2.setWoonplaats("Gouda");
            locatiesRepo.saveAndFlush(loc2);

        }
    }

    public List<Locaties> findEntity(String postcode) {
        if (postcode == null || postcode.trim().length() == 0) {
            return new ArrayList<>();
        }
        Optional<List<Locaties>> optloc = locatiesRepo.findLocatie(postcode);
        if (!optloc.isPresent()){
           return new ArrayList<>();
        }
        return optloc.get();
    }


}
