package nl.yeswayit.flow.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Slf4j
@Service
public class MessageService {

    public String getMsg() {
        return String.format("Hi there!! it's %s here..",
                LocalDateTime.now().format(
                        DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
    }
}
