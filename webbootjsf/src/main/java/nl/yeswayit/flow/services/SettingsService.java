package nl.yeswayit.flow.services;

import nl.yeswayit.shared.utils.ResourceUtilityReader;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class SettingsService {
    private final String CUSTOM_PROP = "custom";

    private final Properties customProperties;

    public SettingsService(){
        customProperties = ResourceUtilityReader.getProperties(CUSTOM_PROP);
    }


    public String getCustomProperty(String key){
        return customProperties.getProperty(key);
    }
}
