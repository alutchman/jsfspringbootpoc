package nl.yeswayit.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public class BasicErrorResponse {
    private Integer status;
    private Date timestamp;
    private String error;
    private String message;
    private String path;
}
