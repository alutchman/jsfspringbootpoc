package nl.yeswayit.utils;


import nl.yeswayit.jsonrest.data.tables.Webuser;
import nl.yeswayit.shared.transfer.UserLevel;
import nl.yeswayit.shared.transfer.WebUserData;

import java.util.Date;

public final class TransferUtils {
    private TransferUtils(){
        //===SINGLETON
    }



    public static WebUserData fromWebuser(Webuser webuser){
        WebUserData webUserData = new WebUserData();
        webUserData.setUserid(webuser.getUserid());
        webUserData.setAchternaam(webuser.getAchternaam());
        webUserData.setVoornaam(webuser.getVoornaam());

        webUserData.setCurrentlevel(UserLevel.REPORT);

        String groupType = webuser.getGroupnaam();
        for (UserLevel userLevel : UserLevel.values()) {
            if (userLevel.name().equals(groupType)) {
                webUserData.setCurrentlevel(userLevel);
            }
        }

        webUserData.setValidUser(true);
        return webUserData;
    }



}
